<?php

abstract class Character
{
    protected $name;
    protected $healthPoints;
    protected $weapon;
    protected $class;
    protected $dodge;
    protected $attacksCount = 1;
    protected $armor;
    protected $mageResist;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function addWeapon($weapon)
    {
        $this->weapon = $weapon;
        return $this;
    }

    public function addArmor($armor)
    {
        $this->armor = $armor;
        return $this;
    }

    public function attack($character)
    {       
        for ($count = 0; $count < $this->weapon->attacksCount(); $count++){
            $miss = rand(0, $character->getDodge());
            if ($miss > 20){
                echo "<b>" . $this->name . ' missed ' . $character->getName() . "</b><br>";
            } else {               
                $character->getHit($this);
            }    
        }    
    } 

    public function getHit($character)
    {
        if ($character->weapon->damageType() == 'phisical') {
            $damage = $character->weapon->getDamage() - $this->armor->getArmorResist();
        } elseif ($character->weapon->damageType() == 'magic') {
            $damage = $character->weapon->getDamage() - $this->armor->getMageResist();
        }

        $this->healthPoints = $this->healthPoints - $damage;
        
        echo $character->getName() . ' attacks ' . $this->name . ' with ' . $character->weapon->getName() . ' ( ' . $character->weapon->damageType() . ' damage:' . $damage .' )<br/>';
        
        echo "__" . $this->name . ' got ' . $damage . ' damage. HP left: ' . $this->healthPoints . '<br/>';       
        return $this->healthPoints;
    }

    public function getName()
    {
        return $this->name;
    }

    public function alive()
    {
        return $this->healthPoints > 0;
    }

    public function getHealthPoints()
    {
        return $this->healthPoints;
    }

    public function getDodge()
    {
        return $this->dodge;
    }

    public function addBag($bag)
    {
        $this->bag = $bag;
        return $this;
    }

    public function BagAmount()
    {
        $amount = $this->bag->getCapicity() -  ($this->armor->getArmorWeight() + $this->weapon->getWeaponWeight());
         return $amount;
    }

    public function Potion(){
        $healthElecsir = $this->BagAmount() / 10;
        return $healthElecsir;
    }

    public function hill()
    {   
            $healka = 10;
           

                $this->healthPoints = $this->healthPoints + $healka;
                echo '<hr>' . $this->getName() . ' DRINKING POTION. HP increases to ' . $this->getHealthPoints() . '<br>';

            
    }
}

class Warrior extends Character
{
    protected $healthPoints = 150;
    protected $dodge = 25;
}

class Rogue extends Character
{
    protected $healthPoints = 80;
    protected $dodge = 40;
}

class Archer extends Character
{
    protected $healthPoints = 75;
    protected $dodge = 35;
}

class Magician extends Character
{
    protected $healthPoints = 75;
    protected $dodge = 10;
}

abstract class Weapon
{
    protected $name;
    protected $minDamage;
    protected $maxDamage;
    protected $attacksCount = 1;
    protected $damageType;
    protected $weight;

    public function getDamage()
    {
        $damage = rand($this->minDamage, $this->maxDamage);
        return $damage;
    }

    public function getName()
    {
        return $this->name;
    }

    public function attacksCount()
    {
        return $this->attacksCount;
    }

    public function damageType()
    {
        return $this->damageType;
    }

    public function getWeaponWeight()
    {
        return $this->weight;
    }    
}

class Sword extends Weapon
{
    protected $name = 'Long Sword';
    protected $minDamage = 10;
    protected $maxDamage = 15;
    protected $attacksCount = 1;
    protected $damageType = 'phisical';
    protected $weight = 20;
}

class Dagger extends Weapon
{
    protected $name = 'Sharp Dagger';
    protected $minDamage = 9;
    protected $maxDamage = 12;
    protected $attacksCount = 1;
    protected $damageType = 'phisical';
    protected $weight = 10;
}

class Bow extends Weapon
{
    protected $name = 'Crossbow';
    protected $minDamage = 7;
    protected $maxDamage = 10;
    protected $attacksCount = 3; 
    protected $damageType = 'phisical';  
    protected $weight = 10;
}

class Staff extends Weapon
{
    protected $name = 'Magic Staff';
    protected $minDamage = 9;
    protected $maxDamage = 12;
    protected $attacksCount = 2; 
    protected $damageType = 'magic'; 
    protected $weight = 20;  
}

class Bag
{
    protected $capacity = 50;

    public function getCapicity()
    {
        return $this->capacity;
    }

    // public function addInBag()
    // {
        
    // }
}

abstract class Armor
{
    protected $armorName;
    protected $armorResist;
    protected $mageResist;
    protected $weight;

    public function getArmorName()
    {
        return $this->armorName;
    }

    public function getArmorResist()
    {
        return $this->armorResist;
    }

    public function getMageResist()
    {
        return $this->mageResist;
    }

    public function getArmorWeight()
    {
        return $this->weight;
    }
}

class Cuirass extends Armor
{
    protected $armorName = 'Iron Cuirass';
    protected $armorResist = 4;
    protected $weight = 20;
}

class Jacket extends Armor
{
    protected $armorName = 'Leather Jacket';
    protected $armorResist = 2;
    protected $weight = 10;
}

class Robe extends Armor
{
    protected $armorName = 'Magic Robe';
    protected $mageResist = 3;
    protected $weight = 10;
}

class Battle
{
    private $winner = null;

    public function __construct($character_1, $character_2)
    {
        $this->character_1 = $character_1;
        $this->character_2 = $character_2;
    }
    
    public function rumble()
    {   
        while (true) {

        $this->character_1->attack($this->character_2);
        $this->character_2->attack($this->character_1);

            
                if ($this->character_2->getHealthPoints() <= 15) {
                    $this->character_2->hill();

            // if ($this->character_1->getHealthPoints() <= 15) {
            //     $this->character_1->hill();
            // }
            }

            if ($this->character_1->alive() == false || $this->character_2->alive() == false) {
                if ($this->character_1->alive()) {
                    $this->winner = $this->character_1;
                }
                if ($this->character_2->alive()) {
                    $this->winner = $this->character_2;
                    
                }
                return $this->winner;
            }
        }       
    }
}

$characterOne = new Warrior('Warrior');

$characterTwo = new Rogue('Rogue');

$weapon_1 = new Sword();

$weapon_2 = new Dagger();

$characterOne->addWeapon($weapon_1);

$characterTwo->addWeapon($weapon_2);

$shineCuirass = new Cuirass();

$brownJacket = new Jacket();

$characterOne->addArmor($shineCuirass);

$characterTwo->addArmor($brownJacket);

$bigBag = new Bag();

$characterOne->addBag($bigBag);

$characterTwo->addBag($bigBag);



// $characterOne->hill();
// $characterTwo->hill();



$fight = new Battle($characterOne, $characterTwo);

$winner = $fight->rumble();     

if ($winner == null) {
    echo "Draw";
} else {
    echo "Character " . $winner->getName() . " won!";
}